import { Route, BrowserRouter as Router, Routes } from "react-router-dom";
import { Home } from "./pages/Home";
import { PhotoDownload } from "./pages/PhotoDownload";
import { ButtonStyles } from "./pages/ButtonStyles";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/photo/:id" element={<PhotoDownload />} />
        <Route path="/buttons" element={<ButtonStyles />} />
      </Routes>
    </Router>
  );
}

export default App;

import { skipToken, useQuery } from "@tanstack/react-query";
import { photoSchema } from "../schemas/photoSchema";

const fetchPhoto = async (id: string) => {
  const response = await fetch(`https://picsum.photos/id/${id}/info`);
  if (!response.ok) {
    throw new Error("Network response was not ok");
  }
  const data = await response.json();
  return photoSchema.parse(data);
};

export const usePicsumPhoto = (id?: string) => {
  return useQuery({
    queryKey: ["photo", id],
    queryFn: id ? () => fetchPhoto(id) : skipToken,
  });
};

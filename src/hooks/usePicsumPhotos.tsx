import { keepPreviousData, useQuery } from "@tanstack/react-query";
import { photosSchema } from "../schemas/photoSchema";

const fetchPhotos = async (page: number) => {
  const response = await fetch(
    `https://picsum.photos/v2/list?page=${page}&limit=20`
  );
  if (!response.ok) {
    throw new Error("Network response was not ok");
  }
  const data = await response.json();
  return photosSchema.parse(data);
};

export const usePicsumPhotos = (page: number) => {
  return useQuery({
    queryKey: ["photos", page],
    queryFn: () => fetchPhotos(page),
    placeholderData: keepPreviousData,
  });
};

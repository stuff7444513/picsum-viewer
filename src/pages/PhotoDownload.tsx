import { useParams } from "react-router-dom";
import { Button } from "../components/Button";
import { ImageDetail, ImageDetailContainer } from "../components/Images";
import { usePicsumPhoto } from "../hooks/usePicsumPhoto";

export const PhotoDownload = () => {
  const { id } = useParams<{ id: string }>();
  const { data: photo, isLoading, isError } = usePicsumPhoto(id);

  if (!id) return <div>Invalid photo ID</div>;
  if (isLoading) return <div>Loading...</div>;
  if (isError || !photo) return <div>Error loading photo details</div>;

  const handleDownload = async () => {
    try {
      const response = await fetch(photo?.download_url, { mode: "cors" });
      const blob = await response.blob();
      const url = window.URL.createObjectURL(blob);
      const link = document.createElement("a");
      link.href = url;
      link.download = photo.author + ".jpg";
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
      window.URL.revokeObjectURL(url);
    } catch (error) {
      console.error("Error downloading the image:", error);
    }
  };

  return (
    <ImageDetailContainer>
      <ImageDetail
        src={`https://picsum.photos/id/${id}/1000/1000`}
        alt={photo.author}
      />
      <p>Author: {photo.author}</p>
      <p>
        Original Size: {photo.width} x {photo.height}
      </p>
      <Button primary onClick={handleDownload}>
        Download
      </Button>
    </ImageDetailContainer>
  );
};

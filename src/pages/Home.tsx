import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { AuthorName, ImageCard } from "../components/Images";
import { Image } from "../components/Image";
import { usePicsumPhotos } from "../hooks/usePicsumPhotos";
import { Button } from "../components/Button";
import styled from "styled-components";

const GridContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
  gap: 10px;
  padding: 20px;
`;

const ButtonContainer = styled.div`
  width: 100%;
  justify-content: center;
  display: flex;
  gap: 20px;
`;

export const Home = () => {
  const [page, setPage] = useState(() => {
    const savedPage = localStorage.getItem("page");
    return savedPage ? parseInt(savedPage) : 1;
  });

  useEffect(() => {
    localStorage.setItem("page", page.toString());
  }, [page]);

  const { data, isLoading, isError } = usePicsumPhotos(page);

  if (isLoading) return <div>Loading...</div>;
  if (isError) return <div>Error loading photos</div>;

  return (
    <div>
      <GridContainer>
        {data?.map((photo) => (
          <ImageCard key={photo.id}>
            <Link to={`/photo/${photo.id}`}>
              <Image
                src={`https://picsum.photos/id/${photo.id}/200/200`}
                alt={photo.author}
              />
            </Link>
            <AuthorName>{photo.author}</AuthorName>
          </ImageCard>
        ))}
      </GridContainer>
      <ButtonContainer>
        <Button primary onClick={() => setPage(page - 1)} disabled={page === 1}>
          Previous
        </Button>
        <Button
          primary
          onClick={() => setPage(page + 1)}
          disabled={data && data.length < 20}
        >
          Next
        </Button>
      </ButtonContainer>
    </div>
  );
};

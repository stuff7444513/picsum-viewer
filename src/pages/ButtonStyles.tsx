import styled from "styled-components";
import { Button } from "../components/Button";

const FlexContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  gap: 20px;
  padding: 20px;
  background-color: #1e1e1e;
`;

const Section = styled.div`
  margin-bottom: 20px;
`;

const SectionTitle = styled.h2`
  color: white;
`;

const ButtonGroup = styled.div`
  display: flex;
  gap: 10px;
  flex-wrap: wrap;
`;

export const ButtonStyles = () => (
  <FlexContainer>
    <Section>
      <SectionTitle>Primary</SectionTitle>
      <ButtonGroup>
        <Button primary>Button</Button>
        <Button primary withChevron>
          Button
        </Button>
        <Button primary withHeart>
          Button
        </Button>
        <Button primary withHeart withChevron>
          Button
        </Button>
      </ButtonGroup>
    </Section>
    <Section>
      <SectionTitle>Hover</SectionTitle>
      <ButtonGroup>
        <Button primary className="hover">
          Button
        </Button>
        <Button primary withChevron className="hover">
          Button
        </Button>
        <Button primary withHeart className="hover">
          Button
        </Button>
        <Button primary withHeart withChevron className="hover">
          Button
        </Button>
      </ButtonGroup>
    </Section>
    <Section>
      <SectionTitle>Disabled</SectionTitle>
      <ButtonGroup>
        <Button primary disabled>
          Button
        </Button>
        <Button primary withChevron disabled>
          Button
        </Button>
        <Button primary withHeart disabled>
          Button
        </Button>
        <Button primary withHeart withChevron disabled>
          Button
        </Button>
      </ButtonGroup>
    </Section>
    <Section>
      <SectionTitle>Secondary</SectionTitle>
      <ButtonGroup>
        <Button>Button</Button>
        <Button withChevron>Button</Button>
        <Button withHeart>Button</Button>
        <Button withHeart withChevron>
          Button
        </Button>
      </ButtonGroup>
    </Section>
    <Section>
      <SectionTitle>Hover</SectionTitle>
      <ButtonGroup>
        <Button className="hover">Button</Button>
        <Button withChevron className="hover">
          Button
        </Button>
        <Button withHeart className="hover">
          Button
        </Button>
        <Button withHeart withChevron className="hover">
          Button
        </Button>
      </ButtonGroup>
    </Section>
    <Section>
      <SectionTitle>Disabled</SectionTitle>
      <ButtonGroup>
        <Button disabled>Button</Button>
        <Button withChevron disabled>
          Button
        </Button>
        <Button withHeart disabled>
          Button
        </Button>
        <Button withHeart withChevron disabled>
          Button
        </Button>
      </ButtonGroup>
    </Section>
  </FlexContainer>
);

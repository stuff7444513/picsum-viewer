import { useState } from "react";
import styled from "styled-components";

const Placeholder = styled.div`
  width: 200px;
  height: 200px;
  background-color: #f0f0f0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Loader = styled.div`
  border: 4px solid #f3f3f3;
  border-top: 4px solid #3498db;
  border-radius: 50%;
  width: 24px;
  height: 24px;
  animation: spin 2s linear infinite;

  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;

const StyledImage = styled.img<{ isLoaded: boolean }>`
  max-width: 100%;
  height: auto;
  display: ${(props: { isLoaded: boolean }) =>
    props.isLoaded ? "block" : "none"};
`;

interface ImageProps {
  src: string;
  alt: string;
}

export const Image = ({ src, alt }: ImageProps) => {
  const [isLoaded, setIsLoaded] = useState(false);

  const handleLoad = () => {
    setIsLoaded(true);
  };

  return (
    <>
      {!isLoaded && (
        <Placeholder>
          <Loader />
        </Placeholder>
      )}
      <StyledImage
        src={src}
        alt={alt}
        isLoaded={isLoaded}
        onLoad={handleLoad}
        onError={handleLoad}
      />
    </>
  );
};

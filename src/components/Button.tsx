import { ButtonHTMLAttributes, ReactNode } from "react";
import { FaChevronRight, FaHeart } from "react-icons/fa";
import styled, { css } from "styled-components";

const primaryStyles = css`
  background-color: #7630cf;
  color: white;
  &:hover:not(:disabled),
  &.hover {
    color: #cdcdcd;
    background-color: #9a64df;
  }
  &:disabled {
    background-color: #8e859b;
    color: #cdcdcd;
  }
`;

const secondaryStyles = css`
  background-color: transparent;
  color: white;
  border: 2px solid white;
  &:hover:not(:disabled),
  &.hover {
    background-color: #efefef20;
  }
  &:disabled {
    background-color: #ac9ec0;
    color: #8654cf;
    border-color: transparent;
  }
`;

const StyledButton = styled.button<{ primary: boolean }>`
  box-sizing: border-box;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  padding: 12px 30px;
  font-size: 16px;
  border: none;
  border-radius: 5px;
  cursor: pointer;
  transition: background-color 0.3s ease, color 0.3s ease,
    border-color 0.3s ease;
  ${({ primary }) => (primary ? primaryStyles : secondaryStyles)}
  &:disabled {
    cursor: not-allowed;
    opacity: 0.6;
  }
`;

const IconLeft = styled.span`
  margin-right: 8px;
  display: flex;
`;

const IconRight = styled.span`
  margin-left: 8px;
  display: flex;
`;

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  primary?: boolean;
  disabled?: boolean;
  withHeart?: boolean;
  withChevron?: boolean;
  onClick?: () => void;
  children?: ReactNode;
}

export const Button = ({
  primary,
  disabled,
  withHeart,
  withChevron,
  children,
  ...props
}: ButtonProps) => (
  <StyledButton primary={!!primary} disabled={disabled} {...props}>
    {withHeart && (
      <IconLeft>
        <FaHeart />
      </IconLeft>
    )}
    {children}
    {withChevron && (
      <IconRight>
        <FaChevronRight />
      </IconRight>
    )}
  </StyledButton>
);

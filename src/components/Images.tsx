import styled from "styled-components";

export const ImageCard = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const AuthorName = styled.p`
  margin-top: 10px;
  font-size: 14px;
  color: #555;
`;

export const ImageDetailContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px;
`;

export const ImageDetail = styled.img`
  max-width: 500px;
  max-height: 500px;
  width: auto;
  height: auto;
`;

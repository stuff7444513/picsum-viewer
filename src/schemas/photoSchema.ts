import { z } from "zod";

export const photoSchema = z.object({
  id: z.string(),
  author: z.string(),
  width: z.number(),
  height: z.number(),
  url: z.string(),
  download_url: z.string(),
});

export const photosSchema = z.array(photoSchema);

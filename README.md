# Picsum image viewer

Retrieve and display images from the Picsum API. clicking on an image opens a larger view with the option to download the image.

Button styles page at `/buttons`.

## Running locally

1. Clone the repository
2. Run `nvm install` to install the correct node version
3. Run `npm install` to install dependencies
4. Run `npm run dev` to start the development server

## TODO

- Testing

- Home page styling
